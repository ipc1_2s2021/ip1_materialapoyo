import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { BackendService } from 'src/app/services/backend.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  response:any

  product: Product = {
    id:0,
    nombre:'',
    precio:0,
    cantidad:0,
    tipo:''
  }; 

  constructor(private backend:BackendService) {
    this.backend.getProducts().toPromise()
      .then(product => {
        this.response = product
        this.product = this.response.data[1]
        console.log(this.product)
        console.log(this.response.data)
      })
      .catch(() => alert("Hubo un problema"))
   }

  ngOnInit(): void {
  }

}
