from flask import Flask, request
from flask.wrappers import Response
from flask_cors import CORS
import otherfile

app = Flask(__name__)
CORS(app)

class Producto:
    id = 1
    nombre = ""
    precio = 0
    cantidad = 0
    tipo = ""

misProductos = list()

@app.route("/", methods = ['GET'])
def api():
    return {"message":otherfile.hola_mundo()}

@app.route("/producto", methods = ['POST'])
def post_product():
    content = request.get_json()

    newProduct = Producto()
    newProduct.id = content['id']
    newProduct.nombre = content['nombre']
    newProduct.precio = content['precio']
    newProduct.cantidad = content['cantidad']
    newProduct.tipo = content['tipo']

    misProductos.append(newProduct)

    return {"message":"Insertado correctamente"}
    
@app.route("/producto", methods = ['GET'])
def get_product():

    response = []

    for p in misProductos:
        pd = {
            "id": p.id,
            "nombre": p.nombre,
            "precio": p.precio,
            "cantidad": p.cantidad,
            "tipo": p.tipo
        }
        response.append(pd)

    return {"status":200,"data":response}

@app.route("/producto", methods = ['PUT'])
def put_product():
    return ""

@app.route("/producto", methods = ['DELETE'])
def delete_product():
    return ""

if __name__ == '__main__':
    app.run(host= 'localhost')
    app.run(debug=True)