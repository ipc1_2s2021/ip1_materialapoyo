/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_objetosv2;

/**
 *
 * @author William Corado
 */
public class Cliente extends Persona{
    String nit;
    String correo;
    
    public Cliente(int codigo, String nombre, boolean genero, String nit,String correo) {
        super(codigo, nombre, genero);
        this.nit = nit;
        this.correo = correo;
    }
    
    public void imprimirAtributos(){
        System.out.println("-------------------------------------------------");
        System.out.println("Su codigo es: "+codigo);
        System.out.println("Su nombre es: "+nombre);
        System.out.println("Su genero es: "+(genero==true ? "Mujer" : "Hombre"));
        System.out.println("Su nit es: "+ nit);
        System.out.println("Su correo es: "+correo);
        System.out.println("-------------------------------------------------");
    }
    
    public void comprar(){
        
    }
}
