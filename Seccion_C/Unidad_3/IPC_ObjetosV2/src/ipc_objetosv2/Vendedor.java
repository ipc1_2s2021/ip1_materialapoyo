/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_objetosv2;

/**
 *
 * @author William Corado
 */
public class Vendedor extends Persona {
    
    int cajas;
    int ventas;
    
    public Vendedor(int codigo, String nombre, boolean genero, int cajas, int ventas) {
        super(codigo, nombre, genero);
        this.cajas = cajas;
        this.ventas = ventas;
    }
    
    public void imprimirAtributos(){
        System.out.println("-------------------------------------------------");
        System.out.println("Su codigo es: "+codigo);
        System.out.println("Su nombre es: "+nombre);
        System.out.println("Su genero es: "+(genero==true ? "Mujer" : "Hombre"));
        System.out.println("Su Cantidad de cajas es: "+cajas);
        System.out.println("Su cantidad de ventas es: "+ventas);
        System.out.println("-------------------------------------------------");
    }
    
    public void vender(){
        
    }

    public int getCajas() {
        return cajas;
    }

    public void setCajas(int cajas) {
        this.cajas = cajas;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }

    @Override
    public int getCodigo() {
        codigo = codigo + 1;
        return super.getCodigo(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
