/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_objetosv2;

/**
 *
 * @author William Corado
 */
public class IPC_ObjetosV2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Vendedor javier = new Vendedor(7, "Javier", false, 25, 30);
        javier.imprimirAtributos();
        Cliente andy = new Cliente(10, "Andy", false, "987321", "andy@gmail.com");
        andy.imprimirAtributos();
    }
    
}
