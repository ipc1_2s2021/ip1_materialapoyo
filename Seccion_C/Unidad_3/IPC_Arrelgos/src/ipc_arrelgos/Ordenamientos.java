/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_arrelgos;

import java.io.FileNotFoundException;

/**
 *
 * @author William Corado
 */
public class Ordenamientos {

    int [] desorden = {3,5,4,9,7,1,5,6,2};
    public Ordenamientos() throws FileNotFoundException {
        //desorden = BubbleSort(desorden);
        insercionDirecta(desorden);
        impresion(desorden);
        reporteHTML html = new reporteHTML(desorden);
    }
    
    public int[] BubbleSort(int[] arreglo){
        int aux;
                
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = 1; j < arreglo.length-i; j++){
                if(arreglo[j-1] > arreglo[j]){
                    aux = arreglo[j-1];
                    arreglo[j-1] = arreglo[j];
                    arreglo[j] = aux;
                }
            }
        }
                
        return arreglo;
    }
    
    public void insercionDirecta(int[]arreglo){
        int p,j;
        int aux;
        for (p = 1; p < arreglo.length; p++) {
            aux = arreglo[p];
            j = p - 1;
            while((j >= 0) && (aux < arreglo[j])) {
                arreglo[j+1] = arreglo[j];
                j--;
            }
            arreglo[j+1] = aux;
        }
    }
    
    public void impresion(int[] arreglo){
        for(int i = 0; i<arreglo.length; i++){
            System.out.println(arreglo[i]);
        }
    }
}
