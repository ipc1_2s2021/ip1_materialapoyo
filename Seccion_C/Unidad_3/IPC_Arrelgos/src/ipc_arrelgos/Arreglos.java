/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_arrelgos;

/**
 *
 * @author William Corado
 */
public class Arreglos {

    int [] arreglo1 = new int[5];
    
    public Arreglos() {
        
    }
    
    public void ejecutar(){
        for(int i = 0; i<=4; i++){
            System.out.println("Arreglo en "+i+" es = "+arreglo1[i]);
        }
        
        arreglo1[0] = 1;
        arreglo1[1] = 3;
        arreglo1[2] = 5;
        arreglo1[3] = 7;
        arreglo1[4] = 9;
        
        System.out.println("Impresion despues de llenarlo...... ");
        for(int i = 0; i<=4; i++){
            System.out.println("Arreglo en "+i+" es = "+arreglo1[i]);
        }
        
        System.out.println("Impresion de atras hacia delante........");
        for(int i = 4; i>=0; i--){
            System.out.println("Arreglo en "+i+" es = "+arreglo1[i]);
        }
        
        System.out.println(suma(arreglo1, arreglo1));
    }
    
    public int suma(int []a, int []b){
        return a[4] + b[2];
    }
}
