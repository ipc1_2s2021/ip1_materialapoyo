/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipc_arrelgos;

/**
 *
 * @author William Corado
 */
public class Tablero {

    int contador;
    
    public Tablero() {
        contador = 64;
        imprimirTablero();
    }
    
    public void imprimirTablero(/*int[] arreglo*/){
        for (int i = 0; i < 9; i++) {
            System.out.println("--------------------------");
            for (int j = 0; j < 7; j++) {
                if(!(j+1<7)){
                    System.out.println(" |");
                } else {
                    if(contador>10){
                        System.out.print("| "+contador);
                    } else {
                        System.out.print("|  "+contador);
                    }
                    
                    contador--;
                }   
            }
            for (int j = 0; j < 7; j++) {
                if(!(j+1<7)){
                    System.out.println(" |");
                } else {
                    System.out.print("|   ");
                }   
            }
        }
        System.out.println("--------------------------");
    }
}
