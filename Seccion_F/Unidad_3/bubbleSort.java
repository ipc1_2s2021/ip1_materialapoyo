package com.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Arreglos {

    public static void main(String[] args) {
        // Bubble Sort: Ordenamiento de arreglos
        int[] arregloDesordenado = new int[] {50, 26, 7, 9, 15, 27};
        int[] arregloOrdenado = bubbleSort(arregloDesordenado);

        for (int i = 0; i < arregloOrdenado.length; i++) {
            System.out.print(arregloOrdenado[i] + ' ');
        }
    }

    /**
     * Funcion que recibe un arreglo, lo ordena y lo devuelve.
     * @param array
     * @return
     */
    public static int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for ( int j = 0; j < array.length - i - 1; j++) {
                if (array[j + 1] < array[j]) {
                    // Se guarda el valor en una variable temporal para no perderlo
                    // Ya que en la siguiente linea se sustituye
                    int temporal = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temporal;
                }
            }
        }
        return array;
    }
}
