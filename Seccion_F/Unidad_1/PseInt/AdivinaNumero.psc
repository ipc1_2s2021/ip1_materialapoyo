// El programa consiste en que el usuario intentar� adivinar
// el n�mero aleatorio que el programa ha generado.
// Se le solicitar� un numero al usuario hasta que sea igual al del programa.

Algoritmo AdivinaNumero
	Definir numeroAleatorio, numeroUsuario Como Entero
	numeroAleatorio <- azar(15) + 1
	
	Escribir "Ingresa un numero"
	Leer numeroUsuario
	
	// Hacer que si el numero que ingresa el usuario esta a 3 posiciones Que muestre "Caliente"
	// Si esta a 6 posiciones de distancia que muestre "Tibio"
	// Si esta a mas de 6 que muestre "Frio"
	
	Mientras numeroUsuario <> numeroAleatorio Hacer
		Si numeroUsuario < 1 O numeroUsuario > 15 Entonces
			Escribir "Debe ser un numero entre 1 y 15"
		SiNo
			Escribir "Has fallado, ingresa otro numero"
		Fin Si
		Leer numeroUsuario
	Fin Mientras
	
	Escribir "Felicidades! Has acertado!!"
FinAlgoritmo
