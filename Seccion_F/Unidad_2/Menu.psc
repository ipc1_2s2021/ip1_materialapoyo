// Procedimiento: NO devuelve un valor
Funcion Menu ()
	Escribir "Seleccione una opcion"
	Escribir "1. Factorial"
	Escribir "2. Fibonacci"
	Escribir "3. Numero mayor de una lista"
	Escribir "4. Salir"
Fin Funcion

// Funcion: Devuelve un valor
// Numero mayor de una lista de numeros
Funcion NumMayor <- NumeroMayor ( lista )
	NumMayor <- -1000
	Para i<-1 Hasta 4 Con Paso 1 Hacer
		Si lista[i] > NumMayor Entonces
			NumMayor <- lista[i]			
		Fin Si
	Fin Para
Fin Funcion

// RECURSIVIDAD
// Es una funcion o procemiento QUE SE LLAMA A SI MISMA las veces que sea necesario.
// La recursividad requiere una condicion de salida o de retorno.

// Factorial: Ayuda visual
// https://www.cs.usfca.edu/~galles/visualization/RecFact.html
// 10! = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1
// 10! = 10 * 9!
// 1! = 1
Funcion fact <- Factorial(n)
	Si n <= 1 Entonces
		fact = 1
	SiNo
		fact = Factorial(n -1) * n
	Fin Si
Fin Funcion

// Fibonacci
Funcion fib <- Fibonacci(n)
	Si n = 0 Entonces
		fib <- 0
	SiNo
		Si n = 1 Entonces
			fib = 1
		SiNo 
			fib = Fibonacci(n -1) + Fibonacci(n - 2)
		FinSi
		
	FinSi
FinFuncion

// Realizar un menu de opciones donde el usuario pueda elegir que desea hacer.
// Debe haber una opcion para salir del menu,
// lo que significa que si no se sale debe seguir mostrandose indefinidamente

Algoritmo Ejemplo
	Definir opt Como Entero
	Dimension lista[4]
	
	Repetir
		Menu()
		
		Leer opt
		
		Segun opt Hacer
			1:
				Escribir "Factorial de 10: ", Factorial(10)
			2:
				Escribir "Fibonacci de 10: ", Fibonacci(10)
			3:
				lista[1] <- 1
				lista[2] <- 20
				lista[3] <- 3
				lista[4] <- 4
				Escribir "Numero mayor de la lista: ", NumeroMayor(lista)
			4:
				Escribir "Hasta luego!"
			De Otro Modo:
				Escribir "La opcion no es valida, intentelo de nuevo"
		Fin Segun
	Hasta Que opt = 4
FinAlgoritmo