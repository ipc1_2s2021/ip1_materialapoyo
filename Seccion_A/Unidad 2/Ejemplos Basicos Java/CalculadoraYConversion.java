import java.util.Scanner; // Importar la libreria necesaria para trabajar

public class Main
{
    
    
    
	public static void main(String[] args) {
	    
	    Calculadora();
	    ConvertirNumeroKB_MB();
		Calculadora();
		 
		  
	}
	
	public static void Calculadora(){
		Scanner ejemplo = new Scanner(System.in);
		char salida='n';
		
		while(salida!='y'){
		    
		
		
		System.out.println("Ingrese el numero A");
		double numero_a = ejemplo.nextDouble();
		
		System.out.println("Ingrese el numero B");
		double numero_b = ejemplo.nextDouble();
		
		System.out.println("Ingrese el tipo de operacion");
		char tipo_de_operacion = ejemplo.next().charAt(0);
		
		// Creacion de ifs para realizar la operacion debida
		
		if (tipo_de_operacion =='+'){
		    //then
		    double resultado = numero_a+numero_b;
		    System.out.println("El resultado es: "+resultado);
		}else if(tipo_de_operacion =='-'){
		    double resultado = numero_a-numero_b;
		    System.out.println("El resultado es: "+resultado);
		    
		}else if(tipo_de_operacion =='*'){
		    double resultado = numero_a*numero_b;
		    System.out.println("El resultado es: "+resultado);
		}else if(tipo_de_operacion =='/'){
		    if(numero_b == 0){
		         System.out.println("Sintax Error, Division entre 0 invalida");
		    }else{
		        double resultado = numero_a/numero_b;
		    System.out.println("El resultado es: "+resultado);
		    }
		}else{
		    System.out.println("Operacion ingresada invalida");
		}
		
		 System.out.println("Desea salirse? y/n");
		 salida = ejemplo.next().charAt(0);
		}
	}
	
	public static void ConvertirNumeroKB_MB(){
	    Scanner ejemplo = new Scanner(System.in); // Iniciar la herramienta de leer
	    
		System.out.println("Ingrese el numero en KB");
		double numerokb = ejemplo.nextDouble();
		double mb = numerokb/1024;
		System.out.println("El numero en MEGABYTES es: "+mb);
	}
	
}