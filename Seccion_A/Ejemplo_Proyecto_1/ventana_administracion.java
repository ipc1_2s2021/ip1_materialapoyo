
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.itextpdf.io.IOException;
import com.itextpdf.kernel.pdf.PdfDocument; 
import com.itextpdf.kernel.pdf.PdfWriter; 

import com.itextpdf.layout.Document; 
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;   



import com.google.gson.Gson;

public class ventana_administracion extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	vendedor[] arreglovendedores = new vendedor[20];
	DefaultTableModel mdl;
	
	
	public ventana_administracion(vendedor[] arreglo_inicial) {
		
		arreglovendedores = arreglo_inicial;
		setTitle("Modulo Administracion");
		setLayout(null);
		setSize(760,560);
		
		Pintar_Botones();
		Colocar_tabla();
		setVisible(true);
		
	}
	
	
	public void Pintar_Botones() {
		JButton	B_salir = new JButton("SALIR");
		B_salir.setBounds(625, 25, 100, 40);
		add(B_salir);
		
		ActionListener salir = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				// accion de boton salir
				System.out.println("accción de salir");
			}
		};
		
		B_salir.addActionListener(salir);
		
		JButton	B_crear = new JButton("Crear Individual");
		B_crear.setBounds(410, 100, 150, 40);	
		add(B_crear);
		ActionListener crear = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				// accion de boton salir
				System.out.println("accción de crear");
			}
		};
		B_crear.addActionListener(crear);
		
		
		JButton	B_CargaMasiva= new JButton("Carga Masiva");
		B_CargaMasiva.setBounds(575, 100, 150, 40);	
		add(B_CargaMasiva);
		ActionListener cargaMasiva = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				// accion de boton salir
				System.out.println("accción de carga masiva");
				Carga_masiva();
				serializar_general(arreglovendedores,"vendedores.ser");
				Colocar_datos_tabla();
			}
		};
		B_CargaMasiva.addActionListener(cargaMasiva);
		
		
		JButton	B_Eliminar= new JButton("Eliminar");
		B_Eliminar.setBounds(575, 160, 150, 40);
		add(B_Eliminar);
		ActionListener eliminar = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				// accion de boton salir
				System.out.println("accción de  eliminar");
			}
		};
		
		B_Eliminar.addActionListener(eliminar);
		
		JButton	B_Actualizar= new JButton("Actualizar");
		B_Actualizar.setBounds(410, 160, 150, 40);
		add(B_Actualizar);
		
		ActionListener actualizar = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				// accion de boton salir
				System.out.println("accción de actualizar");
			}
		};
		
		B_Actualizar.addActionListener(actualizar);
		
		
		JButton	B_pdf= new JButton("Reporte PDF");
		B_pdf.setBounds(410, 215, 315, 40);	
		add(B_pdf);
		ActionListener reporte = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent a) {
				// accion de boton salir
				System.out.println("accción de crear reporte");
				reporte_PDF();
			}
		};
		B_pdf.addActionListener(reporte);
		
	}
	
	public void Colocar_tabla() {
		mdl = new DefaultTableModel();
		mdl.addColumn("CODIGO");
		mdl.addColumn("NOMBRE");
		mdl.addColumn("CAJA");
		mdl.addColumn("VENTAS");
		mdl.addColumn("GENERO");
		
		Colocar_datos_tabla();
		
		JTable tabla = new JTable(mdl);
		tabla.setBounds(20,100,350,420);
		JScrollPane scroll = new JScrollPane(tabla);
		scroll.setBounds(20,100,350,400);
		add(scroll);
		
		
		
	}
	
	public void Colocar_datos_tabla() {
		for(int  i = 0;i<arreglovendedores.length;i++) {
			if(arreglovendedores[i] == null) {
				
			}else {
				String [] filanueva = {Integer.toString(arreglovendedores[i].getCodigo()),arreglovendedores[i].getNombre(),Integer.toString(arreglovendedores[i].getCaja()),Integer.toString(arreglovendedores[i].getVentas()),arreglovendedores[i].getGenero()};
				mdl.addRow(filanueva);
			}
		}
	}
	
	
	public void serializar_general(Object[] arreglo,String ruta) {
		try {
			ObjectOutputStream escribir_archivo = new ObjectOutputStream(new FileOutputStream(ruta));
			escribir_archivo.writeObject(arreglo);
			escribir_archivo.close();
			System.out.println("Arreglo serializado");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.io.IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	
	
	public void Carga_masiva() {
		String json="";
		JFileChooser seleccionarArchivo= new JFileChooser();
		File cua;
		
		int op = seleccionarArchivo.showOpenDialog(this);
		if(op == seleccionarArchivo.APPROVE_OPTION) {
			 cua = seleccionarArchivo.getSelectedFile();
		
		
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(cua));
//			BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\wwwed\\Downloads\\mocks_proyecto1\\profesores.json"));

			String linea;
			
			while((linea = br.readLine())!= null) {
				json +=linea;
			}
			
			br.close();
			
		}catch(Exception e) {	}
		
//		catch(FileNotFoundException e) {	}
		
		System.out.println(json);
		
		Gson gson = new Gson();
		vendedor[] p = gson.fromJson(json, vendedor[].class);
	
		
		
		for (int i=0;i<p.length;i++) {
			
			for(int j = 0;j<arreglovendedores.length;j++) {
				if(arreglovendedores[j]!=null) {
					
				}else {
					arreglovendedores[i] = p[i];
					break;
				}
			}	

		}
		
		
		//MetodoGuardarProfe();
		
		}else {
			
		}
		
		
	}
	
	
	
	
	
	
	
	
	public void reporte_PDF() {
		JEditorPane editor = new JEditorPane();
		JFileChooser seleccionarArchivo= new JFileChooser();
		
		int opcion = seleccionarArchivo.showSaveDialog(null);
		if(opcion == seleccionarArchivo.APPROVE_OPTION) {
		
		try {
			
			OutputStream textoSalida = new FileOutputStream(seleccionarArchivo.getSelectedFile());
			PdfWriter writer = new PdfWriter(textoSalida);
			PdfDocument pdf = new PdfDocument(writer);
//		
			
			Document doc = new Document(pdf); 
			doc.setMargins(20, 20, 20, 20);
//	      
		    float [] pointColumnWidths = {100F, 100F, 100F, 100F, 100F};   
		    Table tabla = new Table(pointColumnWidths);   

		    
			tabla.addCell(new Cell().add(new Paragraph("CODIGO")));       
			tabla.addCell(new Cell().add(new Paragraph("NOMBRE")));       
			tabla.addCell(new Cell().add(new Paragraph("CAJA")));       
			tabla.addCell(new Cell().add(new Paragraph("VENTAS")));       
			tabla.addCell(new Cell().add(new Paragraph("GENERO")));       
			
			for (int i=0;i<arreglovendedores.length;i++) {
				
					if(arreglovendedores[i]==null) {
						
					}else {
						tabla.addCell(new Cell().add(new Paragraph(Integer.toString(arreglovendedores[i].getCodigo()))));       
						tabla.addCell(new Cell().add(new Paragraph(arreglovendedores[i].getNombre())));       
						tabla.addCell(new Cell().add(new Paragraph(Integer.toString(arreglovendedores[i].getCaja()))));       
						tabla.addCell(new Cell().add(new Paragraph(Integer.toString(arreglovendedores[i].getVentas()))));       
						tabla.addCell(new Cell().add(new Paragraph(arreglovendedores[i].getGenero())));
					}	
			}
			
			
			doc.add(new Paragraph("Listado vendedores"));
			doc.add(tabla);
			doc.close();
			
		}catch(Exception ex) {
			
		}
			}
	}
	
	
	
	


	

	



	

	
	
	

}
