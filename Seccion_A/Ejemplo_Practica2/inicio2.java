package ejemplopractica2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class inicio2 extends JFrame {

	private JPanel contentPane;
	private JButton botonnuevo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					inicio2 frame = new inicio2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public inicio2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("TorreHanoi");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(157, 27, 109, 39);
		contentPane.add(lblNewLabel);
		
		botonnuevo = new JButton("nuevojuego");
		botonnuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("hola mundo");
			}
		});
		botonnuevo.setBackground(Color.BLUE);
		botonnuevo.setBounds(157, 76, 109, 21);
		contentPane.add(botonnuevo);
		
		JButton botonautomatico = new JButton("Juego automatico");
		botonautomatico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botonautomatico.setBounds(157, 109, 109, 21);
		contentPane.add(botonautomatico);
		
		JButton botontop = new JButton("To 5");
		botontop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botontop.setBounds(157, 149, 109, 21);
		contentPane.add(botontop);
		
		JButton botonconfiguracion = new JButton("Configuracion");
		botonconfiguracion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botonconfiguracion.setBounds(157, 185, 109, 21);
		contentPane.add(botonconfiguracion);
		
		JButton botonsalir = new JButton("salir");
		botonsalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botonsalir.setBounds(275, 185, 85, 21);
		contentPane.add(botonsalir);
	}

}
