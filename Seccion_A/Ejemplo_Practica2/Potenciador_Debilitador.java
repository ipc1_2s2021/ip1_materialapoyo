package ejemplopractica2;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Potenciador_Debilitador extends Thread {

	JPanel panel;
	JButton label_objeto = new JButton();
	Boolean potenciador = true;
	JLabel hora;
	int  tiemporestante = 0;
	public Potenciador_Debilitador(JPanel panel,JLabel hora) { // recibo el panel para poderlo actualizar y a�adir, tambien contador de tiempo para poder aumentar o disminuir
		super();
		this.hora = hora;
		this.panel = panel;
		this.label_objeto.setOpaque(true);
		this.label_objeto.setBackground(Color.green); // en mi caso siempre al inicio son potenciadores, luego ya varia entre potenciador y debilitador
		this.label_objeto.setBounds(50,50,20,20);
		
		label_objeto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(potenciador == true){
                	aumentar(10);
                	
                }else {
                	disminuir(10);
                }

            }
        });

		this.panel.add(label_objeto);
		}
	
		@Override
		public void run() {
			int contador = 0;
			for(int i = 50;i<500;i+=4) {
				
				this.label_objeto.setLocation(50,i);
				
				try {
					this.sleep(50);
					contador+=50;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(contador >= 1000){
                    contador=0;
                } else {
                    continue;
                }

				if(this.potenciador){
                    this.label_objeto.setBackground(Color.red);
                    this.potenciador = false;
                } else {
                    this.label_objeto.setBackground(Color.green);
                    this.potenciador = true;
                }

				
				
			}
			
			this.panel.remove(this.label_objeto);
			this.panel.updateUI();		}
 
		
		public void aumentar(int numero) {
			int timepores = Integer.parseInt(this.hora.getText());
			timepores+=numero;
			this.hora.setText(String.valueOf(timepores));
			this.panel.updateUI();
			System.out.println("Se aumento tiempo");
		}
		
		
		public void disminuir(int numero) {
			int timepores = Integer.parseInt(this.hora.getText());
			timepores-=numero;
			this.hora.setText(String.valueOf(timepores));
			this.panel.updateUI();
			System.out.println("Se disminuyo tiempo");
		}
}
