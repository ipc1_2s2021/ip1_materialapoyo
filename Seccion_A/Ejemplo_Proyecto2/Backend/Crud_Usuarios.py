# Importando la clase del usuario  y sus metodos
from Usuario import Usuario

class Crud_Users:

    def __init__(self):
        self.arreglo_user = []

    # Crear usuario

    def Crear_User(self,correo,password,nombre,nombre_usuario,genero):
        id = len(self.arreglo_user)
        nuevo = Usuario(id, correo, password, nombre, genero, nombre_usuario)
        
        self.arreglo_user.append(nuevo)
        return id

    def Leer_un_usuario(self,correo):
        for search_user in self.arreglo_user:
            if search_user.correo == correo:
                return search_user.retornar()

    def Read_all_users(self):
        all_users = []
        for user in self.arreglo_user:
            all_users.append(user.retornar())
        return all_users

    # Actualizar usuario
    def updateUser(self, correo, pwd, nombre,nombre_usuario,genero):
        for usuario in self.arreglo_user:
            if usuario.correo == correo:
                usuario.correo = correo
                usuario.password = pwd
                usuario.nombre = nombre
                usuario.nombre_usuario = nombre_usuario
                usuario.genero = genero
                return usuario.retornar()
        return None

    def deleteUsuario(self, correo):
        for usuario in self.arreglo_user:
            if usuario.correo == correo:
                self.arreglo_user.remove(usuario)
                return "Eliminado"
        return "No se pudo eliminar"  


    # Login
    def login(self, correo, pwd):
        for usuario in self.arreglo_user:
            if usuario.correo == correo and usuario.password == pwd:
                print('Usuario ' + correo + " loggeado correctamente.")
                return "ok"
        return "No"

    


