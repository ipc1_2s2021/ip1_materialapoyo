package operacionmatrices;

/**
 *
 * @author chechajosue
 */
public class OperacionMatrices {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Multiplicacion de Matrices
        
        int [][] matrizA = {{1,-1,1},{2,2,3},{-2,-3,-1}};
        int [][] matrizB = {{1,0,4},{0,2,5},{1,3,0}};
        
        // matrizA*matrizB
        int [][] resultado = multiplicarMatrices(matrizA, matrizB);
        
        System.out.println("\n\n");
        
        //Imprimiendo el resultado
        for (int i = 0; i < resultado.length; i++) {
            for (int j = 0; j < resultado.length; j++) {
                System.out.print(resultado[i][j] + "    ");
            }
            System.out.println("");
        }
    }
    
    public static int [][] multiplicarMatrices(int [][] A, int [][] B){
        int [][] matrizResultado = new int[A.length][B.length];
        
        // Recorrer las filas de A
        for (int i = 0; i < A.length; i++) {
            
            // Recorrer columnas de A
            for (int j = 0; j < A.length; j++) {
                
                // Recorrer las columnas de B
                for (int k = 0; k < B.length; k++) {
                    matrizResultado[i][j] += A[i][k] * B[k][j];
                    System.out.print(A[i][k] + "*" + B[k][j] + " + ");
                }
                
                System.out.println("= " + matrizResultado[i][j]);
            }
        }
        return matrizResultado;
    }
}
