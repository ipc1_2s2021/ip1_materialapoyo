/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reportes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author chechajosue
 */
public class Reportes {

    /**
     * @param args the command line arguments
     */
    static String[] nombres = {"Juan", "Pepito", "Josue", "Cesar", "Luis", "Simon"};
    static String[] apellidos = {"Lopez", "Diaz", "Perez", "Aguirre", "Roldan", "Aguilar"};
    static int[] edades = {18, 15, 16, 12, 10, 11};

    public static void main(String[] args) throws IOException {
        generarReporte();
    }

    static void generarReporte() throws IOException {

        File archivoHTML;
        FileWriter escritor = null;

        try {

            archivoHTML = new File("reporte.html");
            escritor = new FileWriter(archivoHTML);
            
            // Fecha y hora del reporte
            LocalDate fecha = LocalDate.now();
            LocalTime hora = LocalTime.now();
            String titulo = "IPC1E";

            String contenidoHTML
                    = "<h1 style=\"color: #5e9ca0; text-align: center;\"><span style=\"color: #000000;\"><strong>IPC1E</strong></span></h1>"
                    + "<p style=\"text-align: center;\"><span style=\"color: #000000;\"><strong>" + fecha + " - " + hora + "</strong></span></p>"
                    + "<p>&nbsp;</p>"
                    + "<p>&nbsp;</p>"
                    + "<table class=\"editorDemoTable\" style=\"height: 36px; width: 615px; border-color: black;\" border=\"1\">"
                    + "<thead>"
                    + "<tr style=\"height: 18px;\">"
                    + "<td style=\"width: 235px; height: 18px;\"><span style=\"background-color: #000000; color: #ffffff;\">Nombres</span></td>"
                    + "<td style=\"width: 218px; height: 18px;\"><span style=\"background-color: #000000; color: #ffffff;\">Apellidos</span></td>"
                    + "<td style=\"width: 140px; height: 18px;\"><span style=\"background-color: #000000; color: #ffffff;\">Edades</span></td>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>";
            
            // Generando las filas de la tabla
            for (int i = 0; i < nombres.length; i++) {
                contenidoHTML
                        += "<tr style=\"height: 18px;\">"
                        + "<td style=\"width: 235px; height: 18px;\">" + nombres[i] + "</td>"
                        + "<td style=\"width: 218px; height: 18px;\">" + apellidos[i] + "</td>"
                        + "<td style=\"width: 140px; height: 18px;\">" + edades[i] + "</td>"
                        + "</tr>";
            }

            contenidoHTML += "</tbody>"
                          +  "</table>";

            // Escribiendo contenido de HTML al escritor
            escritor.write(contenidoHTML);
            System.out.println("Se generó el reporte");

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            try {
                escritor.close();
            } catch (Exception e) {
            }
        }
    }

}
