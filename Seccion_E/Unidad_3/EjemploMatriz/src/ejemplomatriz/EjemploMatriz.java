/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplomatriz;

/**
 *
 * @author chechajosue
 */
public class EjemploMatriz {

    static String SEPARADOR_FILAS = "*****************************************************************";
    static String ESPACIOS_VACIO = "       ", ESPACIOS_CONTADOR_1DIG = "      ", ESPACIOS_CONTADOR_2DIG = "     ";
    static String[][] matriz = new String[8][8];

    public static void main(String[] args) {
        llenarMatriz();
        generarTrampas();
        imprimirMatriz();
    }
    
    
    // Metodo un numero entero random entre limite inferior y superior
    public static int numeroRandom(int inf, int sup) {
        return (int) Math.floor(Math.random()*(sup-inf+1) + inf);
    }
    
    public static void generarTrampas(){
        for (int i = 0; i < matriz.length; i++) {
            
            // Maximo de 5 trampas
            // Minimo de 2 trampas
            
            int contador_trampas = 0;
            
            for (int j = 0; j < matriz.length; j++) {
                
                // 1 = TRAMPA
                // 2 = VACIO
                
                // [0,0]
                if(numeroRandom(1, 2) == 1 && (i+j) != 0){
                    matriz[i][j] = "   X   ";
                    contador_trampas++;
                }
                
                // Si ya tenemos las trampas maximas
                if(contador_trampas >= 4){
                    break;
                }
            }
        }
    }
    
    static void llenarMatriz() {
        // Recorre las filas
        for (int i = 0; i < matriz.length; i++) {
            // Recorre las columnas
            for (int j = 0; j < matriz.length; j++) {
                matriz[i][j] = ESPACIOS_VACIO;
            }
        }

        matriz[0][0] = "   x   ";

        // Posicion anterior
        matriz[0][0] = matriz[0][0].replace("x", " ");

        char caracteres[] = matriz[0][1].toCharArray();
        caracteres[3] = 'x';
        String aux = "";
        matriz[0][1] = "   x  ";

        for (int i = 0; i < caracteres.length; i++) {
            aux += caracteres[i];
        }

        matriz[0][1] = aux;
    }

    static void imprimirMatriz() {
        System.out.println("\n\n");

        int contador = 1;

        // De atras para delante
        // for (int i = matriz.length-1; i > 0; i--)
        for (int i = 0; i < matriz.length; i++) {

            System.out.println(SEPARADOR_FILAS);
            int contador_aux = contador + 7;

            for (int j = 0; j < matriz.length; j++) {

//                Operador ternario
//                EXPRESION ? VERDERO : FALSO

//                if(contador > 9){
//                    espacios = ESPACIOS_CONTADOR_2DIG;
//                } else{
//                    espacios = ESPACIOS_CONTADOR_1DIG;
//                }

//                Equivalente a
//                espacios = contador_aux > 9 ? ESPACIOS_CONTADOR_2DIG : ESPACIOS_CONTADOR_1DIG;


                // Si la fila es par
                if ((i + 1) % 2 == 0) {
                    String espacios = contador_aux > 9 ? ESPACIOS_CONTADOR_2DIG : ESPACIOS_CONTADOR_1DIG;
                    System.out.print("|" + espacios + contador_aux);
                    // Se le decrementa 1 posicion
                    contador_aux--;
                } else {
                    String espacios = contador > 9 ? ESPACIOS_CONTADOR_2DIG : ESPACIOS_CONTADOR_1DIG;
                    System.out.print("|" + espacios + contador);
                }

                contador++;
            }

            System.out.println("|");
            
            // Imprimiendo posiciones en matriz
            // Queda pendiente que apliquen lo de las filas pares e impares
            // Para que imprima la fila al revés
            
            for (int m = 0; m < matriz.length; m++) {
                System.out.print("|" + matriz[i][m]);
            }

            System.out.println("|");
        }

        System.out.println(SEPARADOR_FILAS);
    }
}
