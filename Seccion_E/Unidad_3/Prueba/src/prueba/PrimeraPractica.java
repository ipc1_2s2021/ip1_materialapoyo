package prueba;

import java.io.File;
import java.io.FileWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class PrimeraPractica {
    static String SEPARADOR_FILAS ="-------------------------------------------------------------------------";
    static String ESPACIO_VACIO = "       ", ESPACIOS_CONTADOR_1 ="       ", ESPACIOS_CONTADOR_2 ="      ";
    static String [][] matriz = new String[8][8];
    static String [][] posicion = new String[8][8];
    static String [][] matrizAuxiliar = new String[8][8];
    static int posicionJugador =0;
    static boolean jugando = false;
    
    
    public static void main(String[] args) {
//                Scanner sn = new Scanner(System.in);
//                boolean salir = false;
//                int opcion; //Guardaremos la opcion del usuario
                 
                matrizLlena();
                primeraMatriz();
//                
//        while (!salir) {
//            
//            System.out.println("====== Menu ======");
//                System.out.println("1.Iniciar Juego");
//                System.out.println("2.Para Retomar Juego");
//                System.out.println("3.Generar Reportes");
//                System.out.println("4.Salir.");
//
//            try {
// 
//               System.out.println("Ingrese la opcion que desea realizar:");
//                opcion = sn.nextInt();
// 
//                switch (opcion) {
//                    case 1:
//                        System.out.println("Iniciando el juego:");
//                        jugando = true;
//                        posicionJugador=0;
//                        for (int i = 0; i < matrizAuxiliar.length; i++) {
//                            for (int j = 0; j < matrizAuxiliar.length; j++) {
//                                    matrizAuxiliar[i][j]= "       ";
//                                }
//                            }
//                        dado();
//                        
//                        
//                        
//                        break;
//                    case 2:
//                       System.out.println("Retomando juego:");
//                        dado();
//                        
//                        break;
//                    case 3:
//                        System.out.println("Has seleccionado la opcion 3");
//                        break;
//                    case 4:
//                        salir = true;
//                        break;
//                    default:
//                        System.out.println("Solo números entre 1 y 4");
//                }
//            } catch (InputMismatchException e) {
//                System.out.println("Debes insertar un número");
//                sn.next();
//            }
//            posicion[7][7]="   x   ";
//                        
//        }   
    }
    
        static void matrizLlena(){
        // recorriendo filas 
            for(int i = 0; i < matriz.length ; i++) {
            // recorriendo columnas
                for(int j = 0; j < matriz.length ; j ++) {
                    matriz[i][j] = ESPACIO_VACIO;
                }
            }    
        }
       
            static void imprimiendoMatriz(int dado){
                posicionJugador += dado;
                System.out.println("Debes moverte " + dado + " casillas.");
                boolean estaEnTrampa=false;
                System.out.println("\n\n");
                 int contador = (matriz.length * matriz.length) ;
                    for (int i = 0 ; i < matriz.length; i++) {               
                        System.out.println(SEPARADOR_FILAS);    
                    if((i+1)%2==0){                            
                        contador = (contador- matriz.length -1);                       
                        }else{                          
                            contador = (contador- matriz.length +1);
                        }
                    // numeros de casilla del tablero 
                    int contador_2 = contador-7;
                     int contAuxiliar =contador;
                    for (int j = 0; j < matriz.length; j++) {
                    // para la fila par
                    if((i+1)%2==0){
                        String espacios = contador_2 > 9 ? ESPACIOS_CONTADOR_2 : ESPACIOS_CONTADOR_1;
                        
                        if(contador == posicionJugador){
                        
                        System.out.print("| @"+ espacios.substring(0, espacios.length()-2) + contador);
                       
                        }else{
                        
                            System.out.print("|"+ espacios + contador);
                            
                    }
                        contador_2 ++;  
                        contador--;
                    }else{
                        String espacios = contador > 9 ? ESPACIOS_CONTADOR_2 : ESPACIOS_CONTADOR_1;
                        
                        if(contador == posicionJugador){
                        System.out.print("| @"+ espacios.substring(0, espacios.length()-2) + contador);   
                        }else{
                            System.out.print("|"+ espacios + contador);
                            
                    }
                        contador++;
                    }   
                 }              
                System.out.println("|");  
               // PARA LAS TRAMPAS 
                for (int n = 0; n < matriz.length; n++) {
                    
                    if((i+1)%2==0){
                       
                        if(matriz[i][n]=="   #   "){
                         if(contAuxiliar== posicionJugador){
                            estaEnTrampa= true; 
                         }   
                        }
                        contAuxiliar--;
                    }else{
                        if(matriz[i][n]=="   #   "){
                         if(contAuxiliar== posicionJugador){
                            estaEnTrampa= true; 
                         }   
                        }
                        
                        contAuxiliar++;
                    }
                    System.out.print("| " + matriz[i][n]);
                }
                    
                    System.out.println("|");
            }
                        System.out.println(SEPARADOR_FILAS);
                        
                        if(estaEnTrampa){
                            System.out.println("!Has caido en una penalizacion!");
                            //OPERACION FACIL
                            if(posicionJugador>=1 && posicionJugador<= 16){
                                int trampa = (int)( Math.random()*3);
                                if(trampa==0){
                                    calculoUnoLeySeno();
                                }else if(trampa==1){
                                    calculoDosLeySeno();
                                }else if(trampa==2){
                                     calculoTresLeySeno();
                                }
                                //OPERACION MEDIA 
                            }else if(posicionJugador>=17 && posicionJugador<= 40){
                               int trampa = (int)( Math.random()*3);
                                if(trampa==0){
                                    primeraMatriz();
                                }else if(trampa==1){
                                    segundaMatriz();
                                }else if(trampa==2){
                                     terceraMatriz();
                                }
                               // OPERACION DIFICIL  
                            }else if(posicionJugador>=41 && posicionJugador<= 64){
                                int trampa = (int)( Math.random()*3);
                                if(trampa==0){
                                    divisionMatriz();
                                }else if(trampa==1){
                                    divisionMatriz();
                                }else if(trampa==2){
                                     divisionMatriz();
                                }
                            }
                            
                        }
            }
            static void dado(){
                boolean salir = false;
                while(!salir){
                    if(posicionJugador >=64){
                        salir = true;
                        System.out.println("Has terminado el Juego :). ");
                    }else{
                      System.out.println("Para tirar el dado presiona la letra D o presiona P para regresar al Menu:");
                Scanner leer = new Scanner(System.in);
                String D ;
                D = leer.next();
                if(D.equals("D")){
                int dado = 2;
                dado = (int)( Math.random()*5)+2;
                trampas();
                jugando = false;
                imprimiendoMatriz(dado);
                 
                }else{
                    System.out.println("El dado no se a movido");
                    salir = true;
                }   
                    }
            }
                
            }
                   // Suma primera matriz facil 
                static void primeraMatriz(){
                    Scanner leer = new Scanner(System.in);
                    int matrizA[][]= new int [5][5] ;
                    int matrizB[][]= new int[5][5];
                    int matrizUno[][] = {{9,48,5,0,1},{57,8,4,6,14},{0,5,6,78,15},{21,14,8,19,54},{32,20,26,47,12}};
                    int matrizDos[][] = {{9,5,2,1,8},{4,2,3,47,8},{48,55,32,19,6},{7,56,32,14,8},{32,87,0,1,7}};
                    int matrizResultanteSuma [][]= new int[5][5];
                    //imprimiendo matriz A
                    for (int i = 0; i < matrizUno.length ; i++) { // numero de filas
                        for (int j = 0; j < matrizUno.length ; j++) { //numero de columnas 
                            matrizA[i][j] = matrizUno[i][j];
                            // System.out.print(matrizA[i][j]+ "\t");
                        } 
                      // imprimiendo matriz B
                    }
                    for (int i = 0; i <matrizDos.length ; i++) {
                        for (int j = 0; j < matrizDos.length; j++) {
                            matrizB[i][j]= matrizDos[i][j];
                            // System.out.print((matrizB[i][j]) + "\t");
                        }
                     
                    }
                    // sumando las matrices
                    for (int i = 0; i < matrizUno.length; i++) {
                        for (int j = 0; j < matrizDos.length; j++) {
                            matrizResultanteSuma[i][j]= matrizUno[i][j]+ matrizDos[i][j];
                        }
                    }
                   
                    for (int i = 0; i < matrizUno.length; i++) {
                        
                        for (int j = 0; j < matrizUno.length; j++) {
                            System.out.print("[ " +matrizA[i][j]+ " ]"+ "\t");
                        }
                        
                        System.out.print(" +");
                       
                        for (int j = 0; j < matrizDos.length; j++) {
                            System.out.print(" [" + matrizDos[i][j]+ "]"+ "\t");
                        }
                        System.out.print(" =");
                        for (int j = 0; j < matrizResultanteSuma.length; j++) {
                            System.out.print(" [" + matrizResultanteSuma[i][j]+ "]"+ "\t");
                        }
                        
                        System.out.println("");
                    }
                }
                // suma segunda matriz facil 
                static void segundaMatriz(){
                    Scanner leer = new Scanner(System.in);
                    int matrizA[][]= new int [5][5] ;
                    int matrizB[][]= new int[5][5];
                    int matrizUno[][] = {{4,9,7,45,18},{7,51,26,8,38},{48,26,37,21,19},{1,0,6,8,1},{2,19,55,25,15}};
                    int matrizDos[][] = {{0,2,15,1,66},{21,48,62,7,33},{4,88,0,68,4},{25,18,24,1,55},{24,15,36,5,98}};
                    int matrizResultanteSuma [][]= new int[5][5];
                    //imprimiendo matriz A
                    for (int i = 0; i < matrizUno.length ; i++) { // numero de filas
                        for (int j = 0; j < matrizUno.length ; j++) { //numero de columnas 
                            matrizA[i][j] = matrizUno[i][j];
                            // System.out.print(matrizA[i][j]+ "\t");
                        } 
                      
                    }
                    for (int i = 0; i <matrizDos.length ; i++) {
                        for (int j = 0; j < matrizDos.length; j++) {
                            matrizB[i][j]= matrizDos[i][j];
                            // System.out.print((matrizB[i][j]) + "\t");
                        }
                     
                    }
                    // sumando las matrices
                    for (int i = 0; i < matrizUno.length; i++) {
                        for (int j = 0; j < matrizDos.length; j++) {
                            matrizResultanteSuma[i][j]= matrizUno[i][j]+ matrizDos[i][j];
                        }
                    }
                   
                    for (int i = 0; i < matrizUno.length; i++) {
                        
                        for (int j = 0; j < matrizUno.length; j++) {
                            System.out.print("[ " +matrizA[i][j]+ " ]"+ "\t");
                        }
                        
                        System.out.print(" +");
                       
                        for (int j = 0; j < matrizDos.length; j++) {
                            System.out.print(" [" + matrizDos[i][j]+ "]"+ "\t");
                        }
                        System.out.print(" =");
                        for (int j = 0; j < matrizResultanteSuma.length; j++) {
                            System.out.print(" [" + matrizResultanteSuma[i][j]+ "]"+ "\t");
                        }
                        
                        System.out.println("");
                    }
                }
                // suma tercera matriz facil 
                static void terceraMatriz(){
                    Scanner leer = new Scanner(System.in);
                    int matrizA[][]= new int [5][5] ;
                    int matrizB[][]= new int[5][5];
                    int matrizUno[][] = {{0,1,15,5,36},{1,78,65,32,4},{48,66,39,0,55},{14,98,63,20,15},{11,39,84,7,1}};
                    int matrizDos[][] = {{78,25,66,48,98},{0,45,2,3,1},{2,9,14,10,20},{35,87,65,2,32},{25,8,4,9,39}};
                    int matrizResultanteSuma [][]= new int[5][5];
                    //imprimiendo matriz A
                    for (int i = 0; i < matrizUno.length ; i++) { // numero de filas
                        for (int j = 0; j < matrizUno.length ; j++) { //numero de columnas 
                            matrizA[i][j] = matrizUno[i][j];
                            // System.out.print(matrizA[i][j]+ "\t");
                        } 
                      
                    }
                    for (int i = 0; i <matrizDos.length ; i++) {
                        for (int j = 0; j < matrizDos.length; j++) {
                            matrizB[i][j]= matrizDos[i][j];
                            // System.out.print((matrizB[i][j]) + "\t");
                        }
                     
                    }
                    // sumando las matrices
                    for (int i = 0; i < matrizUno.length; i++) {
                        for (int j = 0; j < matrizDos.length; j++) {
                            matrizResultanteSuma[i][j]= matrizUno[i][j]+ matrizDos[i][j];
                        }
                    }
                   
                    for (int i = 0; i < matrizUno.length; i++) {
                        
                        for (int j = 0; j < matrizUno.length; j++) {
                            System.out.print("[ " +matrizA[i][j]+ " ]"+ "\t");
                        }
                        
                        System.out.print(" +");
                       
                        for (int j = 0; j < matrizDos.length; j++) {
                            System.out.print(" [" + matrizDos[i][j]+ "]"+ "\t");
                        }
                        System.out.print(" =");
                        for (int j = 0; j < matrizResultanteSuma.length; j++) {
                            System.out.print(" [" + matrizResultanteSuma[i][j]+ "]"+ "\t");
                        }
                        
                        System.out.println("");
                    }
                }
                
                // empieza codigo para las trampas 
                public static int casillaPenalizacion(int inf, int sup){
                    return(int) Math.floor(Math.random()*(sup-inf+1)+inf);
                }
                static void trampas(){
                    //columnas
                       
                        if(!jugando){
                            
                           matriz= matrizAuxiliar;
                       }else{
                                  for (int i = 0; i < matriz.length; i++) {
                        //filas 
                        int contaTrampas=0;
                        
                        for (int j = 0; j < matriz.length; j++) {
                            if(casillaPenalizacion(1,2)==1){
                                
                               
                                    matriz[i][j]="   #   ";
                                    matrizAuxiliar[i][j]= matriz[i][j];
                                
                                contaTrampas++;
                            }
                            if(contaTrampas>= 4){
                            break;
                            }
                        } 
                    }
                }
            }
                // division matriz
                static void divisionMatriz(){
                    int matrizA[][] = {{10,5,10,4},{5,4,2,6},{7,8,15,3},{6,8,9,2}};
                    int matrizB[][] = {{5,13,9,4},{1,9,6,3},{8,11,69,33},{25,6,7,4}};
                    
                    // matrizA*matrizB
                    int [][] resultado = multiplicacionMatrices(matrizA,matrizB);
                    System.out.println("\n\n");
                    for (int i = 0; i < resultado.length; i++) {
                        for (int j = 0; j < resultado.length; j++) {
                                System.out.print("[  "+resultado[i][j]+ " ]"+"\t");
                        }
                        System.out.println("");
                    }
                }
                public static int [][] multiplicacionMatrices(int[][] A, int[][] B){
                    int [][] matrizResultado = new int [A.length][B.length];
                        // recorre filas de A
                    for (int i = 0; i < A.length; i++) {
                        // recorre filas de A 
                        for (int j = 0; j < A.length; j++) {
                            // recorrer las columnas de B
                            for (int k = 0; k < B.length; k++) {
                                
                                matrizResultado[i][j] += A[i][k] * B[k][j];
                                System.out.print(A[i][k]+"*" +B[k][j]+ "+ ");
                                
                            }
                            System.out.println("matrizResultado: " + matrizResultado[i][j]);
                        }
                    }
                
               return matrizResultado;
             }
                // TRAMPAS FACILES 
                static void calculoUnoLeySeno(){
                    System.out.println("Utilizando Ley de cosenos Encontrar el lado B y los ángulos β y Υ, con los valores dados.");
                    System.out.println("Valores dados:\n" +
                    "Lado A = 15\n" +
                    "Lado C = 20\n" +
                    "Angulo A = 25");
                    System.out.println("Los resultados son:");
                    int ladoA =15;
                    int ladoC = 20;
                    float ladoB;
                    int anguloA =25;
                    double anguloB;
                    
                    // encontrando el lado B
                    double cosA= Math.cos(anguloA);
                    ladoB= (float) Math.sqrt((ladoC*ladoC)+(ladoA*ladoA)-(2*ladoC*ladoA*cosA)); 
                    System.out.println("El lado B mide: " + ladoB); 
                    anguloB = Math.acos((((ladoA*ladoA)-(ladoB*ladoB)-(ladoC*ladoC))/(-2*ladoB*ladoC)));
                    System.out.println("El valor del angulo B: "+ anguloB);
                    System.out.println("El valor del angulo Y es : "+ (180-anguloA-anguloB));
                }
                // TRAMPAS FACILES 
                static void calculoDosLeySeno(){
                    System.out.println("Utilizando Ley de cosenos Encontrar el lado A y los ángulos A y Υ, con los valores dados.");
                    System.out.println("Valores dados:\n" +
                    "Lado B= 10\n" +
                    "Lado C = 25\n" +
                    "Angulo B = 30");
                    System.out.println("Los resultados son:");
                    float ladoA;
                    int ladoC = 25;
                    float ladoB = 10;
                    float anguloA;
                    double anguloB =30;
                    
                    // encontrando el lado B
                    double cosB= Math.cos(anguloB);
                    ladoA= (float) Math.sqrt((ladoC*ladoC)+(ladoB*ladoB)-(2*ladoC*ladoB*cosB)); 
                    System.out.println("El lado A mide: " + ladoA); 
                    anguloA = (float) Math.acos((((ladoA*ladoA)-(ladoC*ladoC)-(ladoB*ladoB))/(-2*ladoB*ladoC)));
                    System.out.println("El valor del angulo A: "+ anguloA);
                    System.out.println("El valor del angulo Y es : "+ (180-anguloA-anguloB));
                }
                static void calculoTresLeySeno(){
                    System.out.println("Utilizando Ley de cosenos Encontrar El Lado C y los ángulos A y β, con los valores dados.");
                    System.out.println("Valores dados:\n" +
                    "Lado A= 18\n" +
                    "Lado B = 25\n" +
                    "Angulo Y = 30");
                    System.out.println("Los resultados son:");
                    float ladoA =18;
                    float ladoC;
                    float ladoB = 25;
                    float anguloA;
                    double anguloB;
                    int anguloY = 30;
                    
                    // encontrando el lado B
                    double cosY= Math.cos(30);
                    ladoC= (float) Math.sqrt((ladoA*ladoA)+(ladoB*ladoB)-(2*ladoA*ladoB*cosY)); 
                    System.out.println("El lado C mide: " + ladoC); 
                    anguloA = (float) Math.acos((((ladoA*ladoA)-(ladoC*ladoC)-(ladoB*ladoB))/(-2*ladoB*ladoC)));
                    System.out.println("El valor del angulo A: "+ anguloA);
                    System.out.println("El valor del angulo B es : "+ (180-anguloA-anguloY));
                }
                // generando reportes 
    @SuppressWarnings("empty-statement")
                static void generarReporte(){
                    File archivoHTML = new File("reporte.html");
                    try{
                        
                        if( archivoHTML.createNewFile()){
                            System.out.println("Se creo correctamente el archivo");
                            
                        }
                        
                        FileWriter escritor = new FileWriter(archivoHTML);
                        String contenidoHTML =
"<h1 style=\"color: #5e9ca0; text-align: center;\"><span style=\"background-color: #ffffff; color: #0000ff;\">Reporte IPC1</span></h1>\n" +
"<h2 style=\"color: #2e6c80; text-align: center;\">Hora&nbsp;&nbsp;</h2>\n" +
"<table class=\"editorDemoTable\" style=\"height: 70px; width: 582px; border-color: azul;\" border=\"1\" width=\"582\">\n" +
"<thead>\n" +
"<tr style=\"height: 17px;\">\n" +
"<td style=\"height: 17px; width: 162.9px;\"><span style=\"color: #0000ff; background-color: #ffffff;\">Nombre</span></td>\n" +
"<td style=\"height: 17px; width: 296.05px;\"><span style=\"color: #0000ff; background-color: #ffffff;\">Apellidos</span></td>\n" +
"<td style=\"height: 17px; width: 102.65px;\"><span style=\"color: #0000ff; background-color: #ffffff;\">Edad</span></td>\n" +
"</tr>\n" +
"</thead>\n" +
"<tbody>\n" +
"<tr style=\"height: 21px;\">\n" +
"<td style=\"height: 21px; width: 162.9px;\">Pablo Javier&nbsp;</td>\n" +
"<td style=\"height: 21px; width: 296.05px;\">Batz Contreras&nbsp;</td>\n" +
"<td style=\"height: 21px; width: 102.65px;\">21</td>\n" +
"</tr>\n" +
"</tbody>\n" +
"</table>";
                        //escritor.write();
                        
                    }catch(Exception e){
                        
                    }finally{
                        
                    }
                }
}                      
     

