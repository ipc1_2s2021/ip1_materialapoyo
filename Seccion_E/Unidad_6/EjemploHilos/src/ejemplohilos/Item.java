/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplohilos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author chechajosue
 */
public class Item extends Thread {
    
    public JButton item = new JButton();
    public JFrame formAgregar;
    public boolean potenciador = true;
    public int x, y, velocidadY;

    public Item(JFrame formAgregar, int x, int y) {
        super();
        this.formAgregar = formAgregar;
        this.x = x;
        this.y = y;
        this.velocidadY = 5;
        
        item.setBounds(x, y, 64, 64);
        item.setBackground(Color.green);
        item.setIcon(new ImageIcon(getClass().getResource("/imagenes/cuadrado.png")));
        
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Clic");
                if(potenciador){
                    System.out.println("Potenciador");
                }else{
                    System.out.println("Debilitador");
                }
            }
        });
        
        formAgregar.add(item);
        formAgregar.repaint();
    }
    
    public void run(){
        
        int contador = 0;
        
        while(true){
            try {
                
                // Tiempo que esta durmiendo el hilo
                sleep(50);
                contador+=50;
                
                this.y += this.velocidadY;
                
                if(this.y >= 540){
                    this.y=0;
                }
                
                actualizarPosicion();
                formAgregar.repaint();
                
                if(this.y >= 540){
                    this.y=0;
                }
                
                if(contador >= 1000){
                    contador=0;
                } else {
                    continue;
                }
                
                if(this.potenciador){
                    item.setBackground(Color.red);
                    this.potenciador = false;
                } else {
                    item.setBackground(Color.green);
                    this.potenciador = true;
                }
            } catch (Exception e) {
            }
        }
    }
    
    public void actualizarPosicion(){
        this.item.setLocation(this.x, this.y);
    }
}
