// Evaluar el mayor de 3 numeros

Algoritmo sin_titulo
	Definir A Como Real
	Definir B Como Real
	Definir C Como Real
	
	Imprimir  'Ingrese el numero A'
	Leer A
	
	Imprimir  'Ingrese el numero B'
	Leer B
	
	Imprimir  'Ingrese el numero C'
	Leer C
	
	Si A > B Y A > C
		Imprimir A, ' es el mayor de los 3'
	SiNo
		Si B > A Y B > C
			Imprimir B, ' es el mayor de los 3'
		SiNo
			Imprimir C, ' es el mayor de los 3'
		FinSi
	FinSi
	
FinAlgoritmo
