/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablasgraficas;

import java.io.*;

/**
 *
 * @author chechajosue
 */
public class Carro implements Serializable {
    
    private int id, puertas;
    private String fabricante;
    private double precio, km_recorridos;
    private boolean enMovimiento;
    
    // Atributo estatico (solamente existe 1 por todos los objetos)
    public static int contador_carros = 0;
    
    // Sobrecarga de constructores
    
    // Constructor vacio
    public Carro() {
        this.id = -1;
        this.puertas = 0;
        this.fabricante = "";
        this.precio = 0;
        this.km_recorridos = 0;
        this.enMovimiento = false;
        contador_carros++;
    }
    
    // Constructor con parametros
    public Carro(int id, int puertas, String fabricante, double precio) {
        this.id = id;
        this.puertas = puertas;
        this.fabricante = fabricante;
        this.precio = precio;
        this.km_recorridos = 0;
        this.enMovimiento = false;
        contador_carros++;
    }
    
    public void avanzar(double distancia){
        this.enMovimiento = true;
        this.km_recorridos += distancia;
    }
    
    public void detener(){
        this.enMovimiento = false;
    }

    public double getKm_recorridos() {
        return km_recorridos;
    }

    public boolean isEnMovimiento() {
        return enMovimiento;
    }

    public static int getContador_carros() {
        return contador_carros;
    }
    
    public int getId() {
        return id;
    }

    public int getPuertas() {
        return puertas;
    }

    public String getFabricante() {
        return fabricante;
    }

    public double getPrecio() {
        return precio;
    }

    public void setKm_recorridos(double km_recorridos) {
        this.km_recorridos = km_recorridos;
    }

    public void setEnMovimiento(boolean enMovimiento) {
        this.enMovimiento = enMovimiento;
    }

    public static void setContador_carros(int contador_carros) {
        Carro.contador_carros = contador_carros;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public String [] toArreglo(){
        String retorno [] = {String.valueOf(this.id), this.fabricante, String.valueOf(this.puertas), String.valueOf(this.precio)};
        return retorno;
    }
    
    @Override
    public String toString(){
        return "Carro:\nID: " + this.id + "\nFabricante: " + this.fabricante + "\nPuertas: " + this.puertas + "\nPrecio: Q" + this.precio + "\n";
    }
}