# Pasos deployment Heroku

# Backend (flask)
Antes de iniciar deben tener creada su cuenta en [Heroku](https://signup.heroku.com/login)

## Crear entorno virtual Python

En la carpeta raíz de su servidor de flask correr los siguientes comandos desde la terminal. **Para windows usar python y para linux python3.**

    python -m venv venv/
    source venv/bin/activate

## Instalar paquetes de python

    pip install flask
    pip install flask_cors
    pip install gunicorn

## Generar el archivo de requerimientos

    pip freeze > requirements.txt

Ejemplo de archivo requirements.txt

    click==8.0.3
    Flask==2.0.2
    Flask-Cors==3.0.10
    gunicorn==20.1.0
    itsdangerous==2.0.1
    Jinja2==3.0.2
    MarkupSafe==2.0.1
    six==1.16.0
    Werkzeug==2.0.2

## Crear archivo Procfile

Este comando le indica a Heroku que debe iniciar el servidor web, usando gunicorn. Deben colocar el nombre su aplicación de flask, en este ejemplo es **app.py** y el nombre de la aplicación web que también será app

**Archivo Procfile**

    web: gunicorn app:app

## Crear app en heroku

En el dashboard "New" -> "Create new app"

![enter image description here](https://stackabuse.s3.amazonaws.com/media/deploying-a-flask-application-to-heroku-2.png)

Colocan un nombre para la aplicación

![enter image description here](https://stackabuse.s3.amazonaws.com/media/deploying-a-flask-application-to-heroku-3.png)

# Repositorio

    git init .
    git add requirements.txt Procfile app.py
*(todos los archivos que usen de python)*


**Enlazarlo a la aplicación de heroku**

    heroku login
    heroku git:remote -a nombre-aplicacion
    git push heroku master

