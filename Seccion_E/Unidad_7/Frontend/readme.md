# Pasos deployment Heroku

# Frontend
Antes de iniciar deben tener creada su cuenta en [Heroku](https://signup.heroku.com/login)

## Archivos servidor

En la carpeta raíz de su frontend deben crear dos archivos

**Archivo index.php**

    <?php include_once("home.html"); ?>
   
  

>  Deben renombrar su archivo index.html a home.html

> Si tienen carpetas **vendor**, deben renombrarlas ya que Heroku no las toma en cuenta.
    
**Archivo composer.json**

    {}



## Crear app en heroku

En el dashboard "New" -> "Create new app"

![enter image description here](https://stackabuse.s3.amazonaws.com/media/deploying-a-flask-application-to-heroku-2.png)

Colocan un nombre para la aplicación

![enter image description here](https://stackabuse.s3.amazonaws.com/media/deploying-a-flask-application-to-heroku-3.png)

# Repositorio

    git init .


**Enlazarlo a la aplicación de heroku**

    heroku login
    heroku git:remote -a nombre-aplicacion
    git add .
    git push heroku master

