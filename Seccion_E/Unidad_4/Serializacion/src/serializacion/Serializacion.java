/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author chechajosue
 */
public class Serializacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

//        Carro yaris = new Carro(1, 4, "Toyota", "Yaris", 50000, 0);
//        Carro corolla = new Carro(2, 4, "Toyota", "Corolla", 50000, 0);
//        Carro rav4 = new Carro(3, 4, "Toyota", "Rav4", 50000, 0);
//        
//        // Serializacion de un arreglo
//        Carro carros [] = {yaris, corolla, rav4};
//        serializarArreglo(carros, "carros.dat");
//        Lectura del arreglo serializado
//        Carro carros[] = (Carro[]) leerArregloSerializado("carros.dat");
//        for (Carro carro : carros) {
//            System.out.println(carro.toString());
//        }
        // Leemos el JSON
//        String textJSON = leerArchivo("Carros.json");
//        System.out.println(textJSON);
//        Carro carros[] = leerJSON(textJSON);

//          carros[0].setFabricante("Lexus");
//          serializarArreglo(carros, "carros.dat");
        Carro carros_recuperados[] = (Carro[]) leerArregloSerializado("carros.dat");

        for (Carro carro_recuperado : carros_recuperados) {
            System.out.println(carro_recuperado);
        }
    }

    static void serializarArreglo(Object arreglo[], String ruta) {

        try {
            ObjectOutputStream escribir_archivo = new ObjectOutputStream(new FileOutputStream(ruta));
            escribir_archivo.writeObject(arreglo);
            escribir_archivo.close();
            System.out.println("Se serializó el arreglo");
        } catch (Exception e) {
            System.out.println("Ocurrió un error");
            e.printStackTrace();
        }
    }

    static Object[] leerArregloSerializado(String ruta) {
        try {

            ObjectInputStream leer_archivo = new ObjectInputStream(new FileInputStream(ruta));
            Object arreglo_recuperado[] = (Object[]) leer_archivo.readObject();
            leer_archivo.close();
            System.out.println("\nSe recuperó el arreglo\n");
            return arreglo_recuperado;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    static String leerArchivo(String ruta) throws IOException {

        String texto = "";
        BufferedReader lector = null;

        try {
            File archivo = new File(ruta);
            lector = new BufferedReader(new FileReader(archivo));

            String linea = lector.readLine();

            while (linea != null) {
                texto += linea;
                linea = lector.readLine();
            }
        } catch (Exception e) {

            System.out.println("Ocurrió un error");
            e.printStackTrace();

        } finally {
            if (lector != null) {
                lector.close();
            }
        }

        return texto;
    }

    static Carro[] leerJSON(String textoJSON) {

        Gson gson = new Gson();
        Carro carros[] = gson.fromJson(textoJSON, Carro[].class);

        for (Carro carro : carros) {
            System.out.println(carro.toString());
            System.out.println("");
        }

        return carros;
    }
}
