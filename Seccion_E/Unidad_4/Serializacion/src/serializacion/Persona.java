/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion;
import java.io.Serializable;

/**
 *
 * @author chechajosue
 */
public class Persona implements Serializable {
    
    private int id, edad;
    private String nombre, correo;
    private Carro carros [] = new Carro[3];
    private int contador_carros = 0;
    
    private static int contador_personas = 0;

    public Persona(int id, int edad, String nombre, String correo) {
        this.id = id;
        this.edad = edad;
        this.nombre = nombre;
        this.correo = correo;
        contador_personas++;
    }

    public int getId() {
        return id;
    }

    public int getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public void insertarCarro (Carro carro) {
        
        if(contador_carros >= carros.length){
            System.out.println("Ya no se pueden guardar más carros.");
            return;
        }
        
        // Si tiene espacios disponibles
        this.carros[this.contador_carros] = carro;
        this.contador_carros ++;
        System.out.println("Se guardó el carro " + carro.getId() + " en el garaje de la persona " + this.nombre);
    }

    public void imprimirCarros(){
        for (Carro carro : carros) {
            if(carro != null){
                System.out.println(carro.toString());
                System.out.println("");
            }
        }
    }
}
