/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion;
import java.io.Serializable;

/**
 *
 * @author chechajosue
 */
public class Carro implements Serializable {
    
    private int id, puertas;
    private String fabricante;
    private double precio;

    public static int contador_carros = 0;

    public Carro(int id, int puertas, String fabricante, double precio) {
        this.id = id;
        this.puertas = puertas;
        this.fabricante = fabricante;
        this.precio = precio;
        contador_carros++;
    }
    
    public void bocinar(){
        System.out.println("Bip bip");
    }

    public int getId() {
        return id;
    }

    public int getPuertas() {
        return puertas;
    }

    public String getFabricante() {
        return fabricante;
    }

    public double getPrecio() {
        return precio;
    }

    public static int getContador_carros() {
        return contador_carros;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPuertas(int puertas) {
        this.puertas = puertas;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }


    
    @Override
    public String toString(){
        return "\nFabricante: " + this.fabricante + "\nPrecio: " + this.precio;
    }
}
