/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4;

import java.util.Scanner;

/**
 *
 * @author fernando
 */
public class Clase4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int opcion = 0;
        Scanner lectorNumeros = new Scanner(System.in);

        do {
            System.out.println("Seleccione una opcion: ");
            System.out.println("1. Sumas de numeros.");
            System.out.println("2. Obtener un caracter de una cadena");
            System.out.println("3. Salir");

            try {
                opcion = lectorNumeros.nextInt();
            } catch (Exception e) {
                lectorNumeros.nextLine();
                System.out.println("Ingrese valores numerico");
            }
            //String cadena = lectorCadena.nextLine();
            switch (opcion) {
                case 1:
                    sumaNumero();
                    break;
                case 2:
                    obtenerCaracter();
                    break;
                case 3:
                    System.out.println("Gracias");
                    break;
                default:
                    System.out.println("Seleccione un numero entre 1 y 3");
                    break;
            }

        } while (opcion != 3);

    }

    public static void sumaNumero() {
        int num1 = 0, num2 = 0, sum = 0;

        Scanner lect = new Scanner(System.in);

        try {
            System.out.println("Ingresa el primer numero: ");
            num1 = lect.nextInt();
            System.out.println("Ingrese el segundo numero: ");
            num2 = lect.nextInt();
        } catch (Exception e) {
            lect.nextLine();
            System.out.println("Ingrese un valor numerico");
        }
        sum = num1 + num2;
        System.out.println("El resultado es: " + sum);
    }

    public static void obtenerCaracter() {
        String cadena = "";
        Scanner lect = new Scanner(System.in);
        int posicion = 0;

        try {
            System.out.println("Ingrese la cadena: ");
            cadena = lect.nextLine();
            System.out.println("Ingrese la posicion: ");
            posicion = lect.nextInt();
            System.out.println("El caracter es: " + cadena.charAt(posicion));
        } catch (Exception e) {
            System.out.println("Ingrese los datos correctamente");
            System.out.println("Asegurese que la posicion sea la correcta");
        }

    }

}
