//Pseudocodigo para ver realizar una suma entre 2 numeros
Funcion Sumas
	Definir espacio1 Como Entero
	Definir espacio2 Como Entero
	Definir resultado Como Entero

    //(Opcional)Se puede inicializar los espacios en memoria. 

	Imprimir  'Ingrese el primer numero'
	Leer espacio1
	
	Imprimir  'Ingrese el segundo numero'
	Leer espacio2
	
    resultado = espacio1 + espacio2

    Imprimir Concatenar('El resultado de la suma es: ', resultado)

FinFuncion

//Actividad
//REQUISITO: Terminar las actividades expuestas en el archivo Sumas.alg en esta misma carpeta.
/*
	1. En base al algoritmo mas explicito que planteo en las actividades de Sumas.alg reescriba el pseudocodigo.
	2. En base al algoritmo que utilizo en el ejercicio anterior vuelva a crear el diagrama de flujo.
*/