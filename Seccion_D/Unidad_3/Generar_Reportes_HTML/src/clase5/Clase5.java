/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5;

import java.io.*;

/**
 *
 * @author fernando
 */
public class Clase5 {

    /**
     * @param args the command line arguments
     */
    static int[][] tablero = new int[8][8];

    static int contadorReporte = 0;
    static String[] reportes = new String[6];
    static String[] OperacionesFacil = new String[2];
    static String[] OperacionesIntermedio = new String[2];
    static String[] OperacionesDificil = new String[2];

    public static void main(String[] args) {
        // TODO code application logic here

        int contador = 1;
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                tablero[i][j] = contador;
                contador++;
            }
        }
        boolean derecha = false;
        for (int i = 0; i < tablero.length; i++) {

            if (derecha) {
                for (int j = 0; j < tablero[i].length; j++) {
                    System.out.print(tablero[i][j] + "\t|");
                }
            } else {
                for (int j = tablero[i].length - 1; j >= 0; j--) {
                    System.out.print(tablero[i][j] + "\t|");
                }
            }
            System.out.println("");
            derecha = !derecha;
        }
        escribirArchivo("./miPrimerArchivo.txt", "");
        leerArchivo("./miPrimerArchivo.txt");

        escribirHtml("./miPrimeraPaginaJava.html", "");

    }

    public static void leerArchivo(String ruta) {
        File archivo = null;
        FileReader fr = null;

        BufferedReader br = null;

        try {
            archivo = new File(ruta);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            String contenido = "";

            while ((linea = br.readLine()) != null) {
                contenido = contenido + linea + "\n";
            }
            System.out.println(contenido);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void escribirArchivo(String ruta, String contenido) {   //Este metodo solo es para que sepan como escribir un archivo
        FileWriter fichero = null;
        PrintWriter pw = null;

        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);

            for (int i = 0; i < 10; i++) {
                pw.println("Linea: " + i);
            }

        } catch (FileNotFoundException fn) {
            System.out.println("No existe el archivo o directorio");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fichero != null) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void escribirHtml(String ruta, String contenido) {   // Este metodo es para un archivo html con tablas e iteraciones en las matrices. 
        FileWriter fichero = null;
        PrintWriter pw = null;

        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);

            pw.println("<!DOCTYPE html>");
            pw.println("<html lang=\"en\">");
            pw.println("<head>");
            pw.println("    <meta charset=\"UTF-8\">");
            pw.println("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
            pw.println("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            pw.println("    <link rel=\"stylesheet\" href=\"bootstrap.css\">");
            pw.println("    <title>Document</title>");
            pw.println("</head>");
            pw.println("<body>");

            pw.println("<table class=\"table table-hover\" style=\"witdh: 300px !important\">");
            pw.println("<tbody>");
            boolean derecha = false;
            for (int i = 0; i < tablero.length; i++) {
                pw.println("<tr>");
                if (derecha) {
                    for (int j = 0; j < tablero[i].length; j++) {
                        pw.print("<td>");
                        pw.print(tablero[i][j]);
                        pw.println("</td>");
                    }
                } else {
                    for (int j = tablero[i].length - 1; j >= 0; j--) {
                        pw.print("<td>");
                        pw.print(tablero[i][j]);
                        pw.println("</td>");
                    }
                }
                pw.println("</tr>");
                derecha = !derecha;
            }
            
            pw.println("</tbody>");
            pw.println("</table>");
            pw.println("</body>");
            pw.println("</html>");
        } catch (FileNotFoundException fn) {
            System.out.println("No existe el archivo o directorio");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fichero != null) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void generarReportes(String ruta) {

        FileWriter fichero = null;
        PrintWriter pw = null;

        try {
            fichero = new FileWriter(ruta);
            pw = new PrintWriter(fichero);
            pw.println("<!DOCTYPE html>");
            pw.println("<html lang=\"en\">");
            pw.println("<head>");
            pw.println("    <meta charset=\"UTF-8\">");
            pw.println("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
            pw.println("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
            pw.println("    <link rel=\"stylesheet\" href=\"bootstrap.css\">");
            pw.println("    <title>Document</title>");
            pw.println("</head>");
            pw.println("<body>");
            
            for (int i = 0; i < contadorReporte; i++) {
                pw.println(reportes[i]);
                contadorReporte++;
            }
            
            pw.println("</tbody>");
            pw.println("</table>");
            pw.println("</body>");
            pw.println("</html>");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fichero != null) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
