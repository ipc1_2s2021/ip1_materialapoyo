/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otrolab;

import java.util.Random;

/**
 *
 * @author fernando
 */
public class OtroLab {

    /**
     * @param args the command line arguments
     */
    static int[] arreglo = new int[5];
    //  |  0  |   1  | 2   |  3  |   4  |

    int posicion;                               //Indica la posicion actual del jugador. 

    static int[][] tablero = new int[8][8];    //Encargada de llevar el control de las posiciones
    //  | 0, 0 /0,1  /0, 2 / 0, 3 | 1,0 /1,1  /1, 2 / 1, 3 |

    static int[][] tablero2 = new int[8][8];    //Indica donde se encuentran las penalizaciones

    public static void main(String[] args) {
        // TODO code application logic here
        for (int i = 0; i < arreglo.length; i++) {   //asignacion en forma ascendente
            arreglo[i] = i * 10;
        }
        for (int i = 0; i < arreglo.length; i++) {     //acceder a los datos de forma ascendente
            System.out.println(arreglo[i]);
        }
        for (int i = arreglo.length - 1; i >= 0; i--) {   //asignacion a los datos de forma descendente
            arreglo[i] = i * 10;
        }
        for (int i = arreglo.length - 1; i >= 0; i--) {   //acceder a los datos de forma descendente
            System.out.println(arreglo[i]);
        }
        /////arreglos bidimensionales
        int contador = 1;
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                tablero[i][j] = contador;
                contador++;
            }
        }

        boolean derecha = false;
        for (int i = 0; i < tablero.length; i++) {

            if (derecha) {
                for (int j = 0; j < tablero[i].length; j++) {
                    System.out.print(tablero[i][j] + "\t|");
                }
            } else {
                for (int j = tablero[i].length - 1; j >=0 ; j--) {
                    System.out.print(tablero[i][j] + "\t|");
                }
            }
            System.out.println("");
            derecha = !derecha;
        }
        Random generador = new Random();
        int valor = generador.nextInt(2)+0;
        System.out.println(valor);
        
        for (int i = 0; i < tablero2.length; i++) {
            int contadorPenalizaciones =0;
            for (int j = 0; j < tablero2[i].length; j++) {
                if(contadorPenalizaciones <= 4){
                    tablero2[i][j] = generador.nextInt(2)+0;
                    if(tablero2[i][j] == 1){
                        contadorPenalizaciones++;
                    }
                }else{
                    tablero2[i][j] = 0;
                }
                
            }
        }
        System.out.println("------------------------------------------");
        for (int i = 0; i < tablero2.length; i++) {
            for (int j = 0; j < tablero2[i].length; j++) {
                System.out.print(tablero2[i][j]+"\t|");
            }
            System.out.println("");
        }
        
    }
}
